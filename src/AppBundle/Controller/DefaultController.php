<?php

namespace AppBundle\Controller;

use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    
    
    public function indexAction(Request $request)
    {
        $apiToken = $this->container->getParameter('api_token');
        $apiBaseUrl = $this->container->getParameter('api_url');
        $restClient = $this->container->get('circle.restclient');
        try {
            $response = $restClient->get($apiBaseUrl.'marketplace/tours?access_token='.$apiToken);
            $content_array = json_decode($response->getContent());
           
            
            $parameters["tours"] = $content_array;
            
            
          } catch (Circle\RestClientBundle\Exceptions\OperationTimedOutException $exception) {
            
          }
        
        //$request->getSession()->remove('cartId'); 
        
        return $this->render('@App/plantilla/index.html.twig',$parameters);
    }

    public function redirectAysenAction(Request $request)
    {   
        return $this->redirectToRoute('AysenBase');
    }

    
    public function tiendaAction(Request $request)
    {
        $apiToken = $this->container->getParameter('api_token');
        $apiBaseUrl = $this->container->getParameter('api_url');
        $restClient = $this->container->get('circle.restclient');
        try {
            $response = $restClient->get($apiBaseUrl.'marketplace/products?access_token='.$apiToken.'&tag=sauvignon');
            $content_array = json_decode($response->getContent());
            
            $parameters["products"] = $content_array;
          } catch (Circle\RestClientBundle\Exceptions\OperationTimedOutException $exception) {
            
          }
          
         
        // replace this example code with whatever you need
        return $this->render('@App/plantilla/tienda.html.twig',$parameters);
    }



   
    public function carritoAction(Request $request, $code=null)
    {   
        $restClient = $this->container->get('circle.restclient');
        $apiToken = $this->container->getParameter('api_token');
        $apiBaseUrl = $this->container->getParameter('api_url');
        $cartId = $request->getSession()->get('cartId');
        
        if ($cartId != null){
            $response = $restClient->get($apiBaseUrl.'products/shopping-cart/'.$cartId.'?access_token='.$apiToken);
            $cart = json_decode($response->getContent());
           

            if(count($cart) == 1){
                $cart = $cart[0];
            }
                
            $parameters["cart"] = $cart;
            $parameters["code"] = $request->get('code');
        }else{
            $parameters["cart"] = null;
            $parameters["code"] = null;
        }
        
        
        return $this->render('@App/plantilla/carrito.html.twig',$parameters);
    }

    public function resumenAction(Request $request)
    {
       
        
        $apiToken = $this->container->getParameter('api_token');
        $apiBaseUrl = $this->container->getParameter('api_url');

        $restClient = $this->container->get('circle.restclient');
        
        $shippingResponse = $restClient->get($apiBaseUrl.'shipping-methods?access_token='.$apiToken);
    
        $shippingMethods = json_decode($shippingResponse->getContent());
       
        
        $cartId = $request->getSession()->get('cartId');

        if($cartId == null){
            return $this->redirectToRoute('index');
        }
        
        if ($request->isMethod('POST')) {
            
            $restClient = $this->container->get('circle.restclient');

            $customerObject["formaPago"] = 7;
            $customerObject["currency"] = "CLP";
            $customerObject["method_shipping_id"] = intval($request->get('method_shipping_id'));
          
            $customerObject["costumerName"] = $request->get('costumerName');
            $customerObject["costumerPhone"] = $request->get('costumerPhone');
            $customerObject["costumerEmail"] = $request->get('costumerEmail');
            $customerObject["shippingName"] = $request->get('shippingName');
            $customerObject["shippingRut"] = $request->get('shippingRut');
            $customerObject["shippingCity"] = $request->get('shippingCity');
            $customerObject["shippingCommune"] = $request->get('shippingCommune');
            $customerObject["shippingAddress"] = $request->get('shippingAddress');
            $customerObject["billingName"] = $request->get('shippingName');
            $customerObject["billingRut"] =  $request->get('shippingRut');
            $customerObject["billingCity"] = $request->get('shippingCity');
            $customerObject["billingCommune"] =  $request->get('shippingCommune');
            $customerObject["billingAddress"] = $request->get('shippingAddress');
            
            exit;

            $customer = json_encode($customerObject);
            $apiToken = $this->container->getParameter('api_token');
            $apiBaseUrl = $this->container->getParameter('api_url');
            $response = $restClient->post($apiBaseUrl.'products/shopping-cart/checkout/'.$cartId.'?access_token='.$apiToken,$customer);
            $response = json_decode($response->getContent());

            $response = $response[0];
       
            $cartId = $response->cart_id;
            $cartCode = $response->cart_code;
            $price = $response->converted_total_price;
            $pagoBase64 = base64_encode($cartId."@;".$cartCode."@;".$price);
            
            //$request->getSession()->remove('cartId');
            //$webpayURL = 'https://webpay-colchagua.tourpay.cl/sdk/tbk-normal.php?data='.$pagoBase64;
            $urlBase = $response->payment_url;
            $webpayURL = $urlBase.'?data='.$pagoBase64;
            
            return $this->redirect($webpayURL);
        }
        $response = $restClient->get($apiBaseUrl.'products/shopping-cart/'.$cartId.'?access_token='.$apiToken);
    
        $cart = json_decode($response->getContent()); 
     

        if(count($cart) == 1){
            $cart = $cart[0];
        }
        $parameters["cart"] = $cart;
        $parameters["shippingMethods"] = $shippingMethods;
        // replace this example code with whatever you need
        return $this->render('@App/plantilla/resumen.html.twig', $parameters);
    }

    
    public function productoAction(Request $request,$id)
    {
        $apiToken = $this->container->getParameter('api_token');
        $apiBaseUrl = $this->container->getParameter('api_url');

        $restClient = $this->container->get('circle.restclient');
        $response = $restClient->get($apiBaseUrl.'products/'.$id.'?access_token='.$apiToken);
        $product = json_decode($response->getContent());
        $parameters["product"] = $product;

        return $this->render('@App/plantilla/producto.html.twig',$parameters);
    }
   
    public function pagadoAction(Request $request, $id)
    {
        $restClient = $this->container->get('circle.restclient');
        $apiToken = $this->container->getParameter('api_token');
        $apiBaseUrl = $this->container->getParameter('api_url');
        $response = $restClient->get($apiBaseUrl.'products/shopping-cart/'.$id.'?access_token='.$apiToken);
    
        $cart = json_decode($response->getContent()); 
        
        $parameters["cart"] = $cart[0];
        $request->getSession()->remove('cartId');
        return $this->render('@App/plantilla/pagado.html.twig', $parameters);
    }

    public function rechazoAction(Request $request, $id)
    {
        // replace this example code with whatever you need
        return $this->render('@App/plantilla/rechazo.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
    
   

    public function errorAction(Request $request, $id, $id1)
    {
        // replace this example code with whatever you need
        return $this->render('@App/plantilla/error.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    public function addToCartAction(Request $request)
    {
        $restClient = $this->container->get('circle.restclient');

        $productObject["product_id"] = $request->get('product_id');
        $productObject["variant_id"] = $request->get('variant_id');
        $productObject["quantity_products"] = $request->get('quantity_products');
          
        if($request->getSession()->get('cartId') == null){
            
            $productObject["cart_id"] = null;
        }
        else {
            $productObject["cart_id"] = $request->getSession()->get('cartId');
        }
        
        $product = json_encode($productObject);
        $apiToken = $this->container->getParameter('api_token');
        $apiBaseUrl = $this->container->getParameter('api_url');
        $response = $restClient->post($apiBaseUrl.'products/shopping-cart?access_token='.$apiToken,$product);
        
        $content = json_decode($response->getContent())[0];
 
        $request->getSession()->set('cartId',$content->cart_id);
        return $this->redirectToRoute('carrito');
    }

    public function deleteFromCartAction(Request $request,$saleId){
        $code = $request->get('code');
        $restClient = $this->container->get('circle.restclient');
        $apiToken = $this->container->getParameter('api_token');
        $apiBaseUrl = $this->container->getParameter('api_url');
        $response = $restClient->delete($apiBaseUrl.'products/shopping-cart/sale/'.strval($saleId).'?access_token='.$apiToken);
        
        $cartId = $request->getSession()->get('cartId');
        if ($cartId != null)
        {
            $response = $restClient->get($apiBaseUrl.'products/shopping-cart/'.$cartId.'?access_token='.$apiToken);
    
        $cart = json_decode($response->getContent()); 
       
         if($cart[0]->products_sales==[]){
            $request->getSession()->remove('cartId');
         }
        }
        return $this->redirectToRoute('carrito', array('code'=>$code));
    }

    public function createCartSessionAction(Request $request, $cartId){
        $request->getSession()->set('cartId',$cartId);
        $response = new Response('Cart session created! ', Response::HTTP_OK);
        return $response;
    }

    public function removeCartSessionAction(Request $request){
        $request->getSession()->remove('cartId');
        $response = new Response('Cart session removed! ', Response::HTTP_OK);
        return $response;
    }
    

   


    public function checkoutAction(Request $request){
        $restClient = $this->container->get('circle.restclient');
        $apiToken = $this->container->getParameter('api_token');
        $apiBaseUrl = $this->container->getParameter('api_url');

        $response = $restClient->get($apiBaseUrl.'marketplace/products/applicate-coupon/'.$cartId.'/'.$code.'?access_token='.$apiToken);
    }


  

    public function toursAction(Request $request )
    {
        $apiToken = $this->container->getParameter('api_token');
        $apiBaseUrl = $this->container->getParameter('api_url');
        $restClient = $this->container->get('circle.restclient');
        try {
            $response = $restClient->get($apiBaseUrl.'marketplace/tours?access_token='.$apiToken);
            $content_array = json_decode($response->getContent());
            
            $parameters["tours"] = $content_array;
          } catch (Circle\RestClientBundle\Exceptions\OperationTimedOutException $exception) {
            
          }

        // replace this example code with whatever you need
        return $this->render('@App/plantilla/tours.html.twig',$parameters);
    }

    #listar todos los productos
    public function productosAction(Request $request)
    {
        $apiToken = $this->container->getParameter('api_token');
        $apiBaseUrl = $this->container->getParameter('api_url');
        $restClient = $this->container->get('circle.restclient');
        try {
            $response = $restClient->get($apiBaseUrl.'marketplace/products?access_token='.$apiToken);
            $content_array = json_decode($response->getContent());
            
            $parameters["products"] = $content_array;
          } catch (Circle\RestClientBundle\Exceptions\OperationTimedOutException $exception) {
            
          }

        // replace this example code with whatever you need
        return $this->render('@App/plantilla/productos.html.twig',$parameters);
        #-----------------------------------------------------------            
    }

    public function productsByEmpresaMarketplaceAction(Request $request, $empresaId)
    {
        $apiToken = $this->container->getParameter('api_token');
        $apiBaseUrl = $this->container->getParameter('api_url');
        $restClient = $this->container->get('circle.restclient');
        try {
            $response = $restClient->get($apiBaseUrl.'marketplace/'.$empresaId.'/products?access_token='.$apiToken);
            $content_array = json_decode($response->getContent());

            $url = $restClient->get($apiBaseUrl.'marketplace/toursUrl/'.$empresaId.'?access_token='.$apiToken);
            $content_url = json_decode($url->getContent());
            
            $parameters["products"] = $content_array;
            $parameters["url"] = $content_url;
         
          } catch (Circle\RestClientBundle\Exceptions\OperationTimedOutException $exception) {
            
          }

        // replace this example code with whatever you need
        return $this->render('@App/plantilla/productosPorEmpresa.html.twig',$parameters);
    }

    public function tourByEmpresaMarketplaceAction(Request $request, $empresaId)
    {
        $apiToken = $this->container->getParameter('api_token');
        $apiBaseUrl = $this->container->getParameter('api_url');
        $restClient = $this->container->get('circle.restclient');
        try {
            $response = $restClient->get($apiBaseUrl.'marketplace/toursUrl/'.$empresaId.'?access_token='.$apiToken);            
            $content_array = json_decode($response->getContent());            
            $parameters["tours"] = $content_array;
       
            
           
          } catch (Circle\RestClientBundle\Exceptions\OperationTimedOutException $exception) {
            
          }

        // replace this example code with whatever you need
        return $this->render('@App/plantilla/TourPorEmpresa.html.twig',$parameters);
    }

    public function checkProductVariantAction(Request $request)
    {
        $apiToken = $this->container->getParameter('api_token');
        $apiBaseUrl = $this->container->getParameter('api_url');
        $restClient = $this->container->get('circle.restclient');

        $variantName = $request->query->get('variantName');
        $productId = $request->query->get('productId');
        
        try {
            $response = $restClient->get($apiBaseUrl.'products/check/variant?variantName='.$variantName.'&productId='.$productId.'&access_token='.$apiToken);
            $content_array = json_decode($response->getContent());
            
            $parameters["products"] = $content_array;
          } catch (Circle\RestClientBundle\Exceptions\OperationTimedOutException $exception) {
            
          }

          return new JsonResponse($content_array);
    }

    public function calculateDynamicShippingAction(Request $request)
    {
        $apiToken = $this->container->getParameter('api_token');
        $apiBaseUrl = $this->container->getParameter('api_url');
        $restClient = $this->container->get('circle.restclient');

        $cartId = $request->query->get('cartId');
        $shippingMethod = $request->query->get('shippingMethod');
        
        try {
            $response = $restClient->get($apiBaseUrl.'products/shopping-cart/calculate-dynamic-shipping/'.$cartId.'/'.$shippingMethod.'?access_token='.$apiToken);
            $content_array = json_decode($response->getContent());
            
            $parameters["shipping_value"] = $content_array;
          } catch (Circle\RestClientBundle\Exceptions\OperationTimedOutException $exception) {
            
          }

          return new JsonResponse($content_array);
    }

    

    


}

